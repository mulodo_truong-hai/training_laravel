1. Install laravel dusk
https://laravel.com/docs/5.7/dusk
=> tiến hành cài đặt bình thường
2. Sau khi cài đặt xong ta có thư mục như sau :
 - Test/Browser
 - DuskTestCase.php
3. Trong file DuskTestCase.php cấu hình webdriver để chạy trình duyệt browse
- Edit DuskTestCase.php
```php
# Path Test\DuskTestCase.php
use CreatesApplication;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        return RemoteWebDriver::create(
            'http://localhost:9515',
            DesiredCapabilities::phantomjs()
        );
    }
```
4. Đăng ký laravel dusk trong provider
```php
    /**
     * Path : app\Provider\AppServiceProvider
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
```
5. Copy file .env thành .env.dusk.local
=> lý do trong môi trường docker đọc file .env.dusk.local
https://laradock.io/#Laravel-Dusk

6.Môi trường docker cần phải cài PhantomJS webdriver
```php
/**
 * Link : https://jorijn.com/browser-testing-met-laravel-dusk-deel-2-phantomjs/
 * Start selenium 
 * /
```
7. Tiến hành chạy laravel dusk để xem coi chạy được chưa?
+ chạy câu lệnh php artisan dusk xem coi ok chưa.
Nếu chạy được thì OK.
Còn không xem coi là lỗi gì
 + Thường có 2 lỗi xảy ra thường xuyên 
   1. Do selenium và phantomJs chạy cùng chung 1 port nên khả năng bị trùng port rất cao.
   2. File config DuskTestCase.php có port không make với phantomJS đang chạy.