<?php

namespace Tests\Feature;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->clickLink('Register')
                    ->assertSee('Register')
                    ->type('name', 'test')
                    ->type('email', 'test@yahoo.com.vn')
                    ->type('password', '123456')
                    ->type('password_confirmation', '123456')
                    ->click('button[type="submit"]')
                    ->assertPathIs('/home')
                    ->assertSee('You are logged in!')
                    ->pause(2000);
        });
    }
}
