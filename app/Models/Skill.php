<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table = "skills";

     protected $fillable = [
        'title', 'description', 'priority', 'created_at', 'updated_at'
    ];

    /*public static function scopePrioritize($query)
    {
        return $query->orderBy('priority', 'asc');
    }*/
}
