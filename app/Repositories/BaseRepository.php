<?php

namespace App\Repositories;

use App\Repositories\Constracts\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;
    protected static $modelClassName;

    public function __call($method, $args)
    {
        return call_user_func_array(
            array($this->model, $method),
            $args
        );
    }

    public static function __callStatic($method, $args)
    {
        return call_user_func_array(
            array(self::$modelClassName, $method),
            $args
        );
    }

    /**
     * Get model form
     *
     * @return void
     */
    public function getModel()
    {
        return $this->model;
    }

    public function getModelClassName()
    {
        return self::$modelClassName;
    }
}
