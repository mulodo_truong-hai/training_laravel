<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row"> 
        <h2 class="text-center">Add departments</h2>
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="table-responsive">
            <form method="POST" action="{{url('admin/departments')}}" accept-charset="UTF-8">
            @csrf
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>       
                        <th>Name</th>
                        <th>Description</th> 
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('code'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </td>
                        <td>
                            <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" required></textarea>

                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center"><button type="submit" class="btn btn-success">{{ __('Save') }}</button></td>
                    </tr>
                </tbody>
            </table>
            </form>
        </div>
    </div>
</div>
