 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

// First test for routing
Route::get('/rooms', function () {
    $rooms = \App\Models\Room::all();
    return view('admin/location/rooms/index', ['rooms' => $rooms]);
});

Route::namespace('Admin')->prefix('admin')->middleware('auth')->group(function () {
    // Controllers Within The "App\Http\Controllers\Admin" Namespace
    // URL prefix by "admin."

    // Location
    Route::namespace('Location')->prefix('location')->group(function () {
        // Controllers Within The "App\Http\Controllers\Admin\Location" Namespace
        // URL prefix by "admin.location."

        Route::resource('rooms', 'RoomController');
    });

    Route::resource('departments', 'DepartmentsController');

    Route::resource('skills', 'SkillController');
});

Route::group(['prefix' => 'profile',  'middleware' => 'auth'], function () {
    Route::get('/', 'ProfileController@show')->name('profile.show');
    Route::get('/edit', 'ProfileController@edit')->name('profile.edit');
    Route::put('/update', 'ProfileController@update')->name('profile.update');
});

# Social Login
Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider')->where('provider','facebook');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->where('provider','facebook');


# Tho Routes
Route::prefix('admin')->middleware('auth')->group(function() {
    Route::group([
        'namespace' => 'Admin'
    ], function() {
        Route::get('/users', 'UserController@index')->name('admin.users');

        Route::group([
            'prefix' => 'datatables',
            'as' => 'datatables::'
        ],function(){
            Route::get('users', 'UserController@datatableUserList')->name('users');
        });
    });



    Route::get('/{route}', 'AdminController@index');
    Route::get('/{route}/{subroute}', 'AdminController@index');
    Route::get('/{route}/{subroute}/{action}', 'AdminController@index');
    Route::get('/{route}/{subroute}/{id}/{action}', 'AdminController@index');
});
