<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Repositories\DepartmentRepository;
use Illuminate\Http\Request;
use App\Models\Departments;
use App\Repositories\Interfaces\DepartmentRepositoryInterface;
use App\Repositories\Eloquents\DepartmentRepositoryEloquent;
use App\Repositories\Eloquents\EloquentRepository;

class DepartmentsController extends Controller
{
    protected $departmentRepositorySimple;
    protected $departmentRepositoryComplex;

    public function __construct(
        DepartmentRepository $departmentRepositorySimple,
        DepartmentRepositoryInterface $departmentRepositoryComplex
    ) {
        $this->departmentRepositorySimple   = $departmentRepositorySimple;
        $this->departmentRepositoryComplex  = $departmentRepositoryComplex;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // use simple repo
        $departments = $this->departmentRepositorySimple::paginate(5);

        return view('admin/departments/index')->with('departments', $departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/departments/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate inputs
        $request->validate([
            'name'          => 'required|string|unique:departments|min:7|max:50|regex:/^[ A-Za-z]+$/',
            'description'   => 'required|string|max:1024'
        ]);

        try {
            // Save new record
            if ($this->departmentRepositorySimple->create($request->all())) {
                // Flash a successful message
                $request->session()->flash('status', [
                'type' => 'success',
                'message' => 'Department was created successful!'
                ]);
            } else {
                throw new \Exception('Cannot store room data');
            }
        } catch (\Exception $e) {
            // Log error
            Log::error($e->getMessage());

            // Flash a error message
            $request->session()->flash('status', [
            'type' => 'danger',
            'message' => 'Cannot store department data'
            ]);
        }
        
        // Back to list page with flash message
        return redirect()->route('departments.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = $this->departmentRepositorySimple::find($id);
        return view('admin.departments.edit', ['departments' => $departments]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validate inputs
        $request->validate([
            'name'          => 'required|string|min:7|max:50|regex:/^[ A-Za-z]+$/|unique:departments,name,'.$id,
            'description'   => 'required|string|max:1024'
        ]);

        try {
            // Save new record
            if ($this->departmentRepositoryComplex->update($id, $request->all())) {
                // Flash a successful message
                $request->session()->flash('status', [
                    'type' => 'success',
                    'message' => 'Department was update successful!'
                ]);
            } else {
                throw new \Exception('Cannot store room data');
            }
        } catch (\Exception $e) {
            // Log error
            Log::error($e->getMessage());

            // Flash a error message
            $request->session()->flash('status', [
            'type' => 'danger',
            'message' => 'Cannot store department data'
            ]);
        }
        
        // Back to list page with flash message
        return redirect()->route('departments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            // Save new record
            if ($this->departmentRepositoryComplex->delete($id)) {
                // Flash a successful message
                $request->session()->flash('status', [
                    'type' => 'success',
                    'message' => 'Department was delete successful!'
                ]);
            } else {
                throw new \Exception('Cannot store room data');
            }
        } catch (\Exception $e) {
            // Log error
            Log::error($e->getMessage());

            // Flash a error message
            $request->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Cannot store department data'
            ]);
        }

        // Back to list page with flash message
        return redirect()->route('departments.index');
    }
}
