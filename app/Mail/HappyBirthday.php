<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Lang;

class HappyBirthday extends Mailable //implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var App\User
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $templateNo = sprintf("%02d", rand(1, 3));
        $subject    = trans('emails.happy_birthday.subject');

        return $this->subject($subject)
                    ->view('emails.users.happybirthday.hb_' . $templateNo)
                    ->with([
                        'user' => $this->user
                    ]);
    }
}
