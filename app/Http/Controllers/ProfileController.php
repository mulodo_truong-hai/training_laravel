<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\CheckOldPassword;
use App\User;

class ProfileController extends Controller
{
    private $data;

    public function __construct()
    {
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $userId = \Auth::id();
        $user = User::findOrFail($userId);
        $this->data['user'] = $user;
        return view('profile.detail', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $userId = \Auth::id();
        $user = User::findOrFail($userId);
        $this->data['user'] = $user;
        return view('profile.edit', $this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'name' => 'required',
            'password_old' => [new CheckOldPassword],
            'email' => 'required|email|unique:users,email,' . \Auth::id()
        ];

        if ($request->get('password') != '') {
            $rules['password'] = 'required|min:5|confirmed|different:password_old';
        }

        $request->validate($rules);

        $user = User::findOrFail(\Auth::id());
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        if ($request->get('password') != '') {
            $user->password = \Hash::make($request->get('password'));
        }
        $user->save();

        return redirect()->route('profileShow')->with('success', __('profile.text_update_success'));
    }
}
