@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detail') }}</div>

                <div class="card-body">

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right font-weight-bold">{{ __('Name') }}</label>

                        <div class="col-md-6" style="margin-top: 8px">
                            <p>{{ $user->name }}</p>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right font-weight-bold">{{ __('Email') }}</label>

                        <div class="col-md-6" style="margin-top: 8px">
                            <p>{{ $user->email }}</p>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right font-weight-bold">{{ __('Created') }}</label>

                        <div class="col-md-6" style="margin-top: 8px">
                            <p>{{ $user->created_at }}</p>
                        </div>
                    </div>

                     <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right font-weight-bold">{{ __('Last Updated') }}</label>

                        <div class="col-md-6" style="margin-top: 8px">
                            <p>{{ $user->updated_at }}</p>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <a href="{{ route( 'profile.edit' )}}" class="btn btn-primary">
                                {{ __('Edit') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
