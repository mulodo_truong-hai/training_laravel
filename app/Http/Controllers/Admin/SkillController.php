<?php

namespace App\Http\Controllers\Admin;

use App\Models\Skill;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Eloquents\SkillRepository;

class SkillController extends Controller
{
    /**
     * @var SkillRepositoryInterface|\App\Repositories\Repository
     */
    protected $skillRepository;

    public function __construct(SkillRepository $skillRepository)
    {
        $this->skillRepository = $skillRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $skills = $this->skillRepository->paginate(3);
        return view('admin/skills/index', ['skills' => $skills]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/skills/create');
    }

    /**
     * Validation title and description 
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validation request
        $validate = $request->validate([
            'title' => 'required|unique:skills|min:2|max:20',
            'description' => 'required|max:1024',
        ]);
        try {
            // Save new record
            if ($this->skillRepository->create($request->all())) {
                // Flash a successfully message
                $request->session()->flash('status', [
                    'type' => 'success',
                    'message' => 'Successfully create the skill!'
                ]);
            } else {
                throw new \Exception('Cannot create skill data');
            }
        } catch (Exception $e) {
            // Log error 
            Log::error($e->getMessage());
            // Flash a error message
            return back()->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Create the skill error!'
            ]);
        }
        // Back to list page with flash message
        return redirect('admin/skills');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Skill $skill)
    {
        return view('admin.skills.edit', compact('skill', $skill));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //var_dump($request);die();
        // Validation request.
        $request->validate([
            'title' => 'required|min:2|max:20|unique:skills,title,'.$id,
            'description' => 'required|max:1024',
        ]);
        try {
            // Update record
            if ($this->skillRepository->update($id, $request->all())) {
                // Flash a update successfully
                $request->session()->flash('status', [
                    'type' => 'success',
                    'message' => 'Successfully update the skill!'
                ]);
            } else {
                throw new \Exception('Cannot update skill data');
            }
        } catch (Exception $e) {
            // Log error
            Log::error($e->getMessage());
            // Flash a update error
            return back()->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Update the skill error!'
            ]);
        }
        // Back to list page with flash message
        return redirect('admin/skills');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            // Delete a record
            if ($this->skillRepository->delete($id)) {
                // Flash message with delete success
                $request = session()->flash('status', [
                    'type' => 'success',
                    'message' => 'Delete create the skill!'
                ]);
            } else {
                throw new \Exception('Cannot delete skill data');
            }
        } catch (Exception $e) {
            // Log error
            Log::error($e->getMessage());
            // Flash a update error
            return back()->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Delete the skill error!'
            ]);
        }
        // Back to list page with flash message
        return redirect('admin/skills');
    }
}
