<?php

namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\RepositoryInterface;

interface RoomRepositoryInterface extends RepositoryInterface
{
    public function emptySeat();
}