<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#search").on("keyup", function(){
            var value = $(this).val().toLowerCase();
            $("#mytable tr").filter(function(){
                $(this).toggle($(this).text().toLocaleLowerCase().indexOf(value) > -1)
            });
        });
    });
</script>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4>List Departments</h4>
            @if (Session::has('status'))
                <div class="alert alert-success">
                    {{ Session::get('status')['type'] }} - 
                    {{ Session::get('status')['message'] }}
                </div>
            @endif
            <div class="table-responsive">
                <table id="mytable" class="table table-bordred table-striped">
                    <thead>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th><a href="{{ URL::to('admin/departments/create') }}" id="add">Add</a></th>
                    </thead>
                    <tbody>
                        @foreach ($departments as $department)
                        <tr>
                            <td>{{ $department->id }}</td>
                            <td>{{ $department->name }}</td>
                            <td>{{ $department->description }}</td>
                            <td><a href="{{ URL::to('admin/departments/' . $department->id . '/edit') }}" id="edit"><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></button></p></a></td>
                            <td>
                                <form action="{{url('admin/departments', $department->id)}}" method="POST">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
                                </form>
                            </td>
                            <td></td>
                        </tr>
                        @endforeach
                        <div class="row">
                            <div class="col-md-9"> {{ $departments->links() }}</div>
                            <div class="col-md-3"><input class="form-control" id="search" name="search" type="text" placeholder="Search..."> </div>
                        </div>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
