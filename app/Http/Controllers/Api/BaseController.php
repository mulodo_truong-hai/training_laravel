<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function info()
    {
        return [
            'active' => '',
            'menus' => [
                '/admin/users' => 'Users'
            ]
        ];
    }
}
