<?php
namespace App\Repositories\Eloquents;

use App\Repositories\Eloquents\EloquentRepository;

class SkillRepository extends EloquentRepository
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return \App\Models\Skill::class;
    }
}