<?php

namespace App\Console\Commands;

use App\Models\Room;
use App\Event\RoomUpdated;
use Illuminate\Console\Command;

class UpdateRoom extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:room';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update room';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $room = Room::find(1);

        event(new RoomUpdated($room));

    }
}
