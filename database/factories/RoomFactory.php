<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Room::class, function (Faker $faker) {
    return [
        'code' => $faker->randomNumber(2),
        'description' => $faker->paragraph,
        'capacity' => $faker->numberBetween(1, 100),
        'admin_user_id' => $faker->numberBetween(1, 5),
    ];
});
