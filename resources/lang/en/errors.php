<?php

return [
    'text_404_title' => '404 Page not found',
    'text_404_header' => 'The requested url was not found !..',
    'text_404_description' => 'Sorry!Evidently the document you were looking for has either been moved or no longer exist,,',
    'text_500_title' => 'An Error Has Occured',
    'text_500_header' => 'An error has occurred',
    'text_500_description' => 'Server is currently under high load - please hit \'reload\' on your browser in a minute to try again'
];
