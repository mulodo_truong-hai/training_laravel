<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\User;

class UserRepository extends BaseRepository
{
    public function __construct(User $usermodel) {
        $this->model = $usermodel;
        self::$modelClassName = get_class($usermodel);
    }

    /**
     * draw: 1
     * columns[0][data]: id
     * columns[0][name]: id
     * columns[0][searchable]: true
     * columns[0][orderable]: true
     * columns[0][search][value]:
     * columns[0][search][regex]: false
     * columns[1][data]: name
     * columns[1][name]: name
     * columns[1][searchable]: true
     * columns[1][orderable]: true
     * columns[1][search][value]:
     * columns[1][search][regex]: false
     * columns[2][data]: email
     * columns[2][name]: email
     * columns[2][searchable]: true
     * columns[2][orderable]: true
     * columns[2][search][value]:
     * columns[2][search][regex]: false
     * columns[3][data]: created_at
     * columns[3][name]: created_at
     * columns[3][searchable]: true
     * columns[3][orderable]: true
     * columns[3][search][value]:
     * columns[3][search][regex]: false
     * columns[4][data]: updated_at
     * columns[4][name]: updated_at
     * columns[4][searchable]: true
     * columns[4][orderable]: true
     * columns[4][search][value]:
     * columns[4][search][regex]: false
     * order[0][column]: 0
     * order[0][dir]: asc
     * start: 0
     * length: 10
     * search[value]:
     */
    public function getDataTableUsers( Request $request ) {
        $users = $this->model;
        $columns = $request->input( 'columns', array());
        $searching = $request->input('search', array('value' => null));
        if (!empty($searching['value'])) {
            foreach($columns as $column) {
                if ( empty($column['data']) && $column['searchable'] != true) {
                    continue;
                }
                $users = $users->orWhere($column['data'], 'like', '%' . $searching['value'] . '%');
            }
        }
        return $users;
    }

    public function filterUsers($users, Request $request) {
        $columns = $request->input( 'columns', array());
        foreach ($request->input('order', array()) as $index => $order) {
            $orderBy = array_get($columns, (int)$order['column'], false);
            if ($orderBy === false) {
                continue;
            }
            $users = $users->orderBy($orderBy['data'], $order['dir']);
        }
        return $users
            ->limit($request->input('length', 10))
            ->offset($request->input('start', 0))
            ->get();
    }
}
