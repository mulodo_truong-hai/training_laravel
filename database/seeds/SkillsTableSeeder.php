<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skillNames = ['Java', '.NET', 'PHP', 'Python', 'Ruby'];

        foreach ($skillNames as $skillName) {
            factory(App\Models\Skill::class)->create([
                'title' => $skillName
            ]);
        }
    }
}
