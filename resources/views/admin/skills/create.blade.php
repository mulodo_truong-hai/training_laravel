@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><span>Add Skill</span> <a class="btn btn-primary" style="float: right;" href=".">Back</a></div>
                <!-- @foreach ($errors->all() as $error)
                    <div class="btn btn-danger alert alert-{{ session('status.type') }} alert-dismissable">
                        {{$error}}
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                @endforeach -->
                <form style="width: 80%; margin: 0 auto;" method="post" action="{{url('admin/skills')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{ old('title') }}" id="title" name="title" aria-describedby="title" placeholder="Enter title" require>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Decription</label>
                        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" id="title" name="description" aria-describedby="decription" placeholder="Enter decription" require>{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button style="margin-left: 50%; margin-bottom: 10px" type="submit" name="btnsubmit" class="btn btn-primary">Create Skill</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
