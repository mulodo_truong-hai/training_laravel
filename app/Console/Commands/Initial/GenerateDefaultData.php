<?php

namespace App\Console\Commands\Initial;

use App\Models\Module;
use App\Models\ModuleAccessLevel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateDefaultData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:generate_default_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate all default data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->comment("START generate initial values ...");

            // Modules
            $this->comment("Populate Modules ...");
            $modules = $this->getMudules();
            $i = 1;
            foreach ($modules as $code => $name) {
                Module::updateOrCreate(
                    [
                        // Search field(s)
                        'id'    => $i,
                        'code'  => $code
                    ],
                    [
                        // Addition saving field(s)
                        'name'          => $name,
                        'description'   => $name,
                        'order'         => $i++
                    ]
                );
            }

            // Module Access Levels
            $this->comment("Populate Module Access Levels ...");
            $moduleAccessLevels = $this->getModuleAccessLevels();
            $i = 1;
            foreach ($moduleAccessLevels as $code => $name) {
                ModuleAccessLevel::updateOrCreate(
                    [
                        'id'    => $i,
                        'code'  => $code
                    ],
                    [
                        'name'          => $name,
                        'description'   => $name,
                        'value'         => $i,
                        'order'         => $i++
                    ]
                );
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        } finally {
            $this->comment("END generate initial values!!!");
            $this->comment('HURA CHACHACHA!!!');
        }
    }

    private function getMudules()
    {
        return [
            'user'      => 'User',
            'usergrp'   => 'User Group',
            'room'      => 'Room'
        ];
    }

    private function getModuleAccessLevels()
    {
        return [
            'none'      => 'None of all',
            'view'      => 'Read Only',
            'create'    => 'Create and Read',
            'update'    => 'Create, Update',
            'delete'    => 'Create, Update, Delete',
        ];
    }
}
