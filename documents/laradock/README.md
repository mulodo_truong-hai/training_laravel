**LARADOCK DOCUMENTATION**

[TOC]

# Install Laradock
[Follow Official Laradock document to install](http://laradock.io/introduction/#quick-overview) but **STOP** at step
```
docker-compose up -d nginx mysql phpmyadmin redis workspace 
```
And **FOLLOW** bellow steps

# Laradock configurations
## Auto login phpMyadmin
- Link mysql to phpmyadmin
    - File: {$laradock_directory}/docker-compose.yml
    - service: phpmyadmin
    - position: before networks
    - add:
```
services:
  phpmyadmin:
    ...
    links:
      - mysql:db
    networks:
    ....
```
- Change enviroment names
    - File: {$laradock_directory}/docker-compose.yml
    - service: phpmyadmin
    - context: enviroments
    - change:
        - `MYSQL_USER` to `PMA_USER`
        - `MYSQL_PASSWORD` to `PMA_PASSWORD`    

- Change phpMyadmin user
    - File: {$laradock_directory}/.env
    - Context: Credentials/Port
    - Change:
        - `PMA_USER=default` to `PMA_USER={$account_name}`
        - `PMA_PASSWORD=secret` to `PMA_PASSWORD={$account_password}` 
    - Eg:
        - `PMA_USER=root`
        - `PMA_PASSWORD=root`


# Mysql configurations
You can choose mysql version in .env file in Laradock directory.

Eg: `~/Code/Laradock/.env`

- Context: MYSQL
- Change:
    - `MYSQL_VERSION: latest` to `MYSQL_VERSION: {$your_version}`
- Notes: *List supported MySQL version on [Docker hub](https://hub.docker.com/_/mysql/)*


**IF YOU USE MYSQL VERSION LATEST OR larger 8.x PLEASE FOLLOW BELLOW STEPS**

------- BEGIN MYSQL 8.x CONFIGURATION -------

MySQL 8.x has changed authencation method to `caching_sha2_password` so if you want choose MySQL 8.x for your project and your PHP version lower 7.2.4 you must config MySQL authencation method to `mysql_native_password`

## Use `mysql_native_password` plugin for Authencation
- Path: {$Laradock Directory}/mysql/my.cnf
- Service: [mysqld]
- Add:
`default_authentication_plugin=mysql_native_password`

Eg:
```
...
[mysqld]
sql-mode="STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION"
character-set-server=utf8
default_authentication_plugin=mysql_native_password
```
------- END MYSQL 8.x CONFIGURATION -------


# Run project
```
docker-compose up -d nginx mysql phpmyadmin redis workspace 
```
## Test phpMyadmin
Please open http://localhost:8080. If has any error when use MySQL 8.x please [Change MySQL 8.x user password to native](#markdown-header-change-mysql-8.x-user-password-to-native)

## Change MySQL 8.x user password to native
When you change authencation method to mysql_native_password if has any error authencation please [reset MySQL user password to Native](https://bitbucket.org/mulodo_truong-hai/training_laravel/src/master/laradock/MySQL.md#markdown-header-change-password-of-old-accounts-to-native-password-mysql-8x)

**PS**: *Only for MySQL 8.x*

# Laravel Install
## Clone laravel repo
```
git clone git@bitbucket.org:mulodo_truong-hai/training_laravel.git
```
## Access to workspace and go to project
```
docker-compose exec workspace bash
```
### Go to project
```
cd /var/www/{$project_name}
```

### Setup laravel
```
composer update && composer run-script post-root-package-install && composer run-script post-create-project-cmd
```

## [Create user & database if you need](https://bitbucket.org/mulodo_truong-hai/training_laravel/src/master/laradock/MySQL.md#markdown-header-create-new-mysql-user)

## Change MySQL host
- File: {$project_directory}/.env
- Context: DB
- Change:
    - `DB_HOST=127.0.0.1` to `DB_HOST=mysql`

# NGINX setup site
You must setup nginx site for test laravel is working
## Add new site
- File: {$laradock}/nginx/sites/{$site_name}.conf
- Content:

```
server {

    listen 80;
    listen [::]:80;

    server_name {$domain_name};
    root /var/www/{$project_name}/public;
    index index.php index.html index.htm;

    location / {
         try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass php-upstream;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }

    location /.well-known/acme-challenge/ {
        root /var/www/letsencrypt/;
        log_not_found off;
    }

    error_log /var/log/nginx/{$site_name}_error.log;
    access_log /var/log/nginx/{$site_name}_access.log;
}
```
**PS:** You must config `$domain_name` and `$project_name` in NGINX site config

Eg:

- $project_name: laravel_trainning
- $domain_name: laravel.test
- $site_name: laravel_trainning

## Create host name for local
- Windows:
    - File: %windir%\system32\drivers\etc\hosts
    - Position: end of file
    - Add new line: `127.0.0.1  {$domain_name}`
- MacOS:
    - File: /etc/hosts
    - Position: end of file
    - Add new line: `127.0.0.1  {$domain_name}`

**PS**: *`{$domain_name}` must same `{$domain_name}` at step create NGINX site*

Eg: 

- `{$domain_name}` is `laravel.test`

## Reload NGINX service
```
docker-compose exec nginx nginx -s reload
```
