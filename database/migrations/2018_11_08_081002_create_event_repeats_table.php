<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRepeatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_repeats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 32)->unique();
            $table->string('description');
        });

        // Populate default data
        DB::table('event_repeats')->insert([
            'code' => 'ML',
            'description' => 'Monthly'
        ]);
        DB::table('event_repeats')->insert([
            'code' => 'YL',
            'description' => 'Yearly'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_repeats');
    }
}
