<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        \App\Repositories\Interfaces\RoomRepositoryInterface::class => \App\Repositories\Eloquents\RoomRepositoryEloquent::class,
        \App\Repositories\Interfaces\DepartmentRepositoryInterface::class => \App\Repositories\Eloquents\DepartmentRepositoryEloquent::class,
    ];

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
