@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><span>Edit Skill</span> <a class="btn btn-primary" style="float: right;" href="/admin/skills">Back</a></div>
                <!-- @foreach ($errors->all() as $error)
                    <div class="btn btn-danger alert alert-{{ session('status.type') }} alert-dismissable">
                        {{$error}}
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    </div>
                @endforeach -->
                <form style="width: 80%; margin: 0 auto;" method="POST" action="{{url('admin/skills', $skill['id'])}}">
                    @method('PUT')
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{__('Title')}}</label>
                        <input type="text" value="{{ old('title') ? old('title') : $skill->title }}" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" id="title" name="title" aria-describedby="title" placeholder="Enter title" require>
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">{{__('Decription')}}</label>
                        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Enter decription" require>{{ old('description') ? old('description') : $skill->description }}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button style="margin-left: 50%; margin-bottom: 10px" type="submit" name="btnsubmit" class="btn btn-primary">{{ __('Edit Skill') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
