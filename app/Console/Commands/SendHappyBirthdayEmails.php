<?php

namespace App\Console\Commands;

use App\User;
use App\Mail\HappyBirthday;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendHappyBirthdayEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email:happy_birthday';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            info('[HappyBirthday] START...');

            $users = $this->getUsersByBirthday();

            foreach ($users as $user) {
                $sendTo = $user->email;
                $message = (new HappyBirthday($user))->onQueue('emails');

                info("[HappyBirthday] Add email address [{$sendTo}] to sending queue");

                Mail::to($sendTo)->queue($message);
            }

        } catch (\Exception $e) {
            info('[HappyBirthday] Error occurs!');
            Log::error('[HappyBirthday] ' . $e->getMessage());
        } finally {
            info('[HappyBirthday] END!!!');
        }
    }

    /**
     * Get list of users that have birthday on the given day
     *
     * @param date $birthday
     * @return User $users
     */
    private function getUsersByBirthday($birthday = '')
    {
        if (!$birthday) {
            $birthday = Carbon::now()->format('m-d');
        }

        $users = User::whereRaw("DATE_FORMAT(birthday, '%m-%d') = :birthday", [$birthday])
                        //->whereNotNull('email_verified_at')
                        ->whereNotNull('email')
                        ->get();

        return $users;
    }
}
