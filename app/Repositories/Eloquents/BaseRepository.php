<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Contracts\RepositoryInterface;
use App\Models\Departments;

abstract class BaseRepository implements RepositoryInterface
{
    /**
     * var
     */
    protected $model;

    /**
     * List paginate in departments
     *
     * @param string limit
     * @param array columns
     */
    public function paginate($limits = null, $columns = array('*'))
    {
        $limit = is_null($limits) ? null : $limits;
        $results = Departments::paginate($limit, $columns);
        return $results;
    }

    /**
     * Save a new entity in repository
     */
    public function create($input = [])
    {
        $this->model = Departments::create($input);
        return $this->model;
    }

    /**
     *  Edit a new entity in repository
     */
    public function edit($id)
    {
        $this->model = Departments::where('id', '=', $id)
                        ->orderBy('name')
                        ->get();
        foreach ($this->model as $data) {
            $this->model = $data;
        }
        return $this->model;
    }

    /**
     *  Update a entity in repository by id
     */
    public function update($input, $id)
    {
        $this->model = Departments::findOrFail($id);
        $this->model->fill(array(
            'name' => $input->name,
            'description' => $input->description
            ));
        return (string)$this->model->save();
    }

    /**
     * Delete a entity in repository by id
     *
     * @param $id
     *
     * @return int
     */
    public function delete($id)
    {
        $this->model = Departments::destroy($id);
        return $this->model;
    }
}
