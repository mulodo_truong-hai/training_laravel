<?php

namespace App\Repositories;

use App\Models\Departments;

class DepartmentRepository extends Departments
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'departments';
}
