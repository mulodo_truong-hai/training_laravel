<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class ProccessSocialNetwork
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        try {
            if ($event->user instanceof User && \Session::has('social_provider')) {
                $user = $event->user;
                $dataSocial = \Session::get('social_provider');
                $user->provider = $dataSocial['provider'];
                $user->provider_id = $dataSocial['provider_id'];
                $user->avatar_url = $dataSocial['avatar_url'];
                $user->save();
            }
        } catch(\Exception $e) {
            \Log::info($e->getMessage());
        }
    }
}
