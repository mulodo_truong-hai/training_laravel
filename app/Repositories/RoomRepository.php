<?php

namespace App\Repositories;

use App\Models\Room;
use App\User;

class RoomRepository extends Room
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rooms';

    /**
     * Return the empty seat of the room
     */
    public function emptySeat()
    {
        $seated = User::where('room_id', $this->id)->count();

        return $this->capacity - $seated;
    }
}
