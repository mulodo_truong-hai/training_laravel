<?php

namespace App\Repositories\Interfaces;

use App\Repositories\Interfaces\RepositoryInterface;

interface DepartmentRepositoryInterface extends RepositoryInterface
{
    public function all();
}
