<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Skill;
use App\Repositories\Eloquents\SkillRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;

class SkillUnitTest extends TestCase
{
    //use Exception;
    //use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_it_can_create_all_data_skill()
    {
        //data exemple
        $data = [
            'title' => 'php',
            'description' => 'php php php php',
            'priority' => 1,
        ];

        //count record before create
        echo "\n\n" . 'Total record before create: ' . Skill::count() . "\n\n";

        //Call Model by Repository create record
        $skillRepo = new SkillRepository(new Skill);
        $skill = $skillRepo->create($data);
        
        //check value input with database
        $skills = $skillRepo->all();
        foreach ($skills as $value) {
            if ($value['title'] === $data['title']) {
                echo 'true' . "\n\n\n";
            }
        }

        //Test assert 
        $this->assertInstanceOf(Skill::class, $skill);
        $this->assertEquals($data['title'], $skill->title);
        $this->assertEquals($data['description'], $skill->description);
        $this->assertEquals($data['priority'], $skill->priority);

        //count record after create
        echo 'Total record after create: ' . Skill::count() . "\n";
        
    }

    public function test_it_can_create_a_title_skill()
    {
        // Check error query
        $this->expectException(QueryException::class);

        // Data example
        $data = [
            'title' => 'php',
        ];
        
        // Create record with data exemple and thow exception
        $skillRepo = new SkillRepository(new Skill);
        $skill = $skillRepo->create($data);
    }

    public function test_it_can_update_all_skill()
    {
        //data exemple   
        $data = [
            'title' => 'PHP',
            'description' => 'Test PHP',
            'priority' => 1,
        ];

        // count total record before update
        echo "\n\n" . 'Total record before create: ' . Skill::count() . "\n\n";
        
        // Call Model by Repository update record
        $skillRepo = new SkillRepository(new Skill);
        $skill = $skillRepo->update(3, $data);

        // Check data after update.
        var_dump($skillRepo->find(3));

        // Count total record after update
        echo "\n\n" . 'Total record before create: ' . Skill::count() . "\n\n";
        
        // Test Assert
        $this->assertEquals($data['title'], $skill->title);
        $this->assertEquals($data['description'], $skill->description);
        $this->assertEquals($data['priority'], $skill->priority);
    }

    public function test_it_not_can_update_with_null_description_skill()
    {
        // Check error query
        $this->expectException(QueryException::class);

        //data exemple   
        $data = [
            'title' => 'PHP',
            'description' => null,
            'priority' => 1,
        ];

        // Call Model by Repository update record
        $skillRepo = new SkillRepository(new Skill);
        $skill = $skillRepo->update(3, $data);
    }

    public function test_it_not_can_update_with_unique_title_skill()
    {
        // Check error query
        $this->expectException(QueryException::class);

        //data exemple   
        $data = [
            'title' => 'PHP',
            'description' => null,
            'priority' => 1,
        ];

        // Call Model by Repository update record
        $skillRepo = new SkillRepository(new Skill);
        $skill = $skillRepo->update(4, $data);
    }

    public function test_it_can_delete_skill()
    {
        // count total record before update
        echo "\n\n" . 'Total record before create: ' . Skill::count() . "\n\n";

        // Call Model by Repository delete skill
        $skillRepo = new SkillRepository(new Skill);
        // Delete skill with id = 5
        $delete = $skillRepo->delete(5);

        // count total record after update
        echo "\n\n" . 'Total record before create: ' . Skill::count() . "\n\n";

        // Test delete Assert
        $this->assertTrue($delete);
    }
}
