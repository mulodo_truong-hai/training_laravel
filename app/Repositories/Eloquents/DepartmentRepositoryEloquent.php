<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Eloquents\EloquentRepository;
use App\Repositories\Interfaces\DepartmentRepositoryInterface;

class DepartmentRepositoryEloquent extends EloquentRepository implements DepartmentRepositoryInterface
{
    /**
     * @inherite
     */
    public function getModel()
    {
        return \App\Models\Departments::class;
    }
}
