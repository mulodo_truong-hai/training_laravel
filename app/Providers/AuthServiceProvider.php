<?php

namespace App\Providers;

use App\Models\Module;
use App\Models\ModuleAccessLevel;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        \App\User::class => \App\Policies\UserPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //Gate::resource('user', 'App\Policies\UserPolicy');

        if (Schema::hasTable('modules') && Schema::hasTable('module_access_levels')) {
            foreach (Module::all() as $module) {
                foreach (ModuleAccessLevel::all() as $accessLevel) {
                    $gateName = $accessLevel->code . '-' . $module->code;

                    Gate::define($gateName, function ($user) use ($module, $accessLevel) {
                        if ($user->isAdmin()) {
                            return true;
                        }

                        return (int) \Auth::user()->getModuleAccessLevel($module->id) >= $accessLevel->value;
                    });
                }
            }
        }

        //dd(Gate::abilities());
    }
}
