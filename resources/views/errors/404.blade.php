<!DOCTYPE HTML>
<html>
<head>
<title>{{ __('errors.text_404_title') }}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="{{ asset('css/errors/style.css') }}">
<link href='//fonts.googleapis.com/css?family=Condiment' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="wrap">
<div class="main">
	<div class="banner">
		<img src="{{ asset('images/errors/banner.png') }}" alt="" />
	</div>
	<div class="text">
		<h1>{{ __('errors.text_404_header') }}</h1>
		<p>{{ __('errors.text_404_description') }}</p>
	</div>
</div>

</div>
</body>
</html>