<?php

namespace App\Http\Controllers\Admin\Location;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Repositories\Interfaces\RoomRepositoryInterface;
use App\Repositories\Eloquents\RoomRepositoryEloquent;
use App\Repositories\RoomRepository;

class RoomController extends Controller
{
    protected $roomRepositorySimple;
    protected $roomRepositoryComplex;

    public function __construct(
        RoomRepository $roomRepositorySimple,
        RoomRepositoryInterface $roomRepositoryComplex
    ) {
        $this->roomRepositorySimple = $roomRepositorySimple;
        $this->roomRepositoryComplex = $roomRepositoryComplex;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Use Eloquent model
        //$rooms = Room::paginate(5);

        // use simple repo
        $rooms = $this->roomRepositorySimple::paginate(5);

        // use complex repo
        //$rooms = $this->roomRepositoryComplex->paginate(5);

        return view('admin/location/rooms/index')->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/location/rooms/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate inputs
        $request->validate([
            'code'      => 'required|unique:rooms|max:255',
            'capacity'  => 'required|integer',
        ]);

        try {
            // Save new record
            if ($this->roomRepositorySimple->create($request->all())) {
                // Flash a successful message
                $request->session()->flash('status', [
                    'type' => 'success',
                    'message' => 'Room was created successful!'
                ]);
            } else {
                throw new \Exception('Cannot store room data');
            }
        } catch (\Exception $e) {
            // Log error
            Log::error($e->getMessage());

            // Flash a error message
            $request->session()->flash('status', [
                'type' => 'danger',
                'message' => 'Cannot store room data'
            ]);
        }

        // Back to list page with flash message
        return redirect()->route('rooms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function edit(Room $room)
    {
        return view('admin/location/rooms/edit')->with(compact('room'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Room $room)
    {
        $room->save();

        /*
        $request->session()->flash('status', [
            'type' => 'warning',
            'message' => 'Room was updated successful!'
        ]);
        */

        flash('Room was updated successful!')->warning();
        flash('Room was updated successful!')->error();
        flash('Room was updated successful!')->success();

        return redirect()->route('rooms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     * @return \Illuminate\Http\Response
     */
    public function destroy(Room $room)
    {
        //
    }
}
