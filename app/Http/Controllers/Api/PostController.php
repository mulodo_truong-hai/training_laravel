<?php

namespace App\Http\Controllers\Api;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{

    public function index()
    {
        return Post::all();
    }

    public function store(Request $request)
    {

        $postData = (array)$request->all();

        // We will set auth ID when user is authorized via API
        $postData['user_id'] = 1;

        $validator = Validator::make($postData, [
            'title' => 'required|unique:posts|min:10|max:255',
            'content' => 'required',
            'user_id' => 'required'
        ]);

        $post = false;
        if (!$validator->fails()) {
            $post = Post::create($postData);
        }
        $errors = $validator->errors();
        return [
            'post' => $post,
            'errors' => $errors
        ];
    }

    public function update(Request $request, $id)
    {
        $post = Post::find($id);
        if ($post) {
            $rules = [
                'title' => 'required|unique:posts|max:255',
                'content' => 'required',
                'user_id' => 'required'
            ];
            $validateRules = [];
            foreach ($post->getFillable() as $field) {
                if (Input::has($field)) {
                    $post->$field = Input::get($field);
                    if (isset($rules[$field])) {
                        $validateRules[$field] = $rules[$field];
                    }
                }
            }
            $validator = Validator::make($request->all(), $validateRules);
            if (!$validator->fails()) {
                $post->update();
            }
            $errors = $validator->errors();
        }
        return [
            'post' => $post,
            'errors' => $errors
        ];
    }

    public function destroy($id):String
    {
        $post = Post::find($id);
        if ($post) {
            $post->delete();
            return 'deleted';
        } else {
            return 'Error when delete';
        }
    }

    public function show($id)
    {
        $post = Post::find($id);
        if ($post) {
            return $post;
        }
        return [
            'error' => 'Post not found'
        ];
    }
}
