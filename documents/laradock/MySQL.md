[TOC]
# Change password of old accounts to native password (MySQL 8.x)
```
docker-compose exec mysql mysql -u{$mysql_root_account} -p{$mysql_root_password} -e "ALTER USER '{$account_want_update_native_password}'@'%' IDENTIFIED WITH mysql_native_password BY '$new_passord';"
```

Eg:
```
docker-compose exec mysql mysql -rroot -proot -e "ALTER USER 'default'@'%' IDENTIFIED WITH mysql_native_password BY 'secret';"
```
**PS:** Only use for MySQL 8.x

# Create new MySQL user
## For MySQL 8.x
```
docker-compose exec mysql mysql -u{$mysql_root_account} -p{$mysql_root_password} -e "CREATE USER '{$account_want_to_create}'@'%' IDENTIFIED WITH mysql_native_password BY '$mysql_user_passord';"
```

Eg:
```
docker-compose exec mysql mysql -uroot -proot -e "CREATE USER 'homestead'@'%' IDENTIFIED WITH mysql_native_password BY 'secret';"
```
## For MySQL 5.x
```
docker-compose exec mysql mysql -u{$mysql_root_account} -p{$mysql_root_password} -e "CREATE USER '{$account_want_to_create}'@'%' IDENTIFIED BY '$mysql_user_passord';"
```

Eg:
```
docker-compose exec mysql mysql -uroot -proot -e "CREATE USER 'homestead'@'%' IDENTIFIED BY 'secret';"
```

# Create new database
```
docker-compose exec mysql mysql -u{$mysql_root_account} -p{$mysql_root_password} -e "CREATE DATABASE {$database_name};"
```
Eg:
```
docker-compose exec mysql mysql -uroot -proot -e "CREATE DATABASE homestead;"
```

# Grant permission for MySQL user to new exists database
```
docker-compose exec mysql mysql -u{$mysql_root_account} -p{$mysql_root_password} -e "GRANT ALL ON {$data_name}.* TO '{$mysql_user}'@'%'"
```
Eg:
```
docker-compose exec mysql mysql -uroot -proot -e "GRANT ALL ON homestead.* TO 'homestead'@'%'"
```

# Downgrade mysql version from 8.x to 5.x
If you have update lated MySQL version (MySQL 8.x) in Laradock and want to downgrade please follow all bellow steps
- Stop all containers
```
docker-compose down
```

- Remove all MySQL image and children of Mysql images
```
docker images | grep mysql | xargs docker rmi
```
- Change Laradock MySQL version to 5.x
    - Path: {$laradock_directory}/.env
    - Context: MYSQL
    - Change:
        - `MYSQL_VERSION: latest` to `MYSQL_VERSION: 5.7`

- Remove old MySQL data for MySQL 8.x
```
rm ~/.laradock/data/mysql
```
- Re-up Laradock
```
docker-compose up -d nginx mysql php-fpm redis workspace
```