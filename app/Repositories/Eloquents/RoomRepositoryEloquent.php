<?php

namespace App\Repositories\Eloquents;

use App\Repositories\Eloquents\EloquentRepository;
use App\Repositories\Interfaces\RoomRepositoryInterface;

class RoomRepositoryEloquent extends EloquentRepository implements RoomRepositoryInterface
{
    /**
     * @inherite
     */
    public function getModel()
    {
        return \App\Models\Room::class;
    }

    public function emptySeat()
    {
        return 0;
    }
}