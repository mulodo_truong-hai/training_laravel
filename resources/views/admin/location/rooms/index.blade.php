@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            {{-- $rooms->links() --}}
            <div class="card">
                <div class="card-header">Room List</div>

                <div class="card-body">
                    <table border="1">
                        <thead>
                            <th>Id</th>
                            <th>Code</th>
                            <th>Capacity</th>
                            <th>Empty Seat</th>
                            <th>Description</th>
                            <th>Edit</th>
                        </thead>
                        <tbody>
                            @foreach ($rooms as $room)
                            <tr>
                                <td>{{ $room->id }}</td>
                                <td>{{ $room->code }}</td>
                                <td>{{ $room->capacity }}</td>
                                <td>{{ $room->emptySeat() }}</td>
                                <td>{{ $room->description }}</td>
                                <td><a href="{{ route('rooms.edit', $room->id) }}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
