<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row"> 
        <h2 class="text-center">Edit departments</h2>
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="table-responsive">
                <form method="POST" action="{{ URL::to('admin/departments/' . $departments->id )}}" accept-charset="UTF-8">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>        
                            <th>Name</th>
                            <th>Description</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $departments->id }}</td>
                            <td>
                                <input id="name" name="name" required="required" type="text" value="{{$departments->name}}">
                            </td>
                            <td>
                                <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $departments->description }}" required>{{ $departments->description }}</textarea>
                            </td>
                        </tr>
                        <tr>
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <td colspan="3" align="center"><button type="submit" class="btn btn-success">{{ __('Update') }}</button></td>
                        </tr>
                    </tbody>
                </table>
                </form>
            </div>
    </div>
</div>