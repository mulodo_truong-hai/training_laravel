<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->text(50),
        'content' => $faker->paragraph,
        'user_id' => rand(1, 102)
    ];
});
