<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Log;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout',
            'redirectToProvider',
            'handleProviderCallback'
        ]);
    }

    /**
     * Redirect the user to the Social authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }
    
    /**
     * Obtain the user information from Social.
     * Check if the user already exists in the database by looking up 
     * their provider_id in the database. If the user exists, log them in.
     * Otherwise, direct to create a new user page.
     * 
     * //@param Request $request
     * //@param UserRepository $userRepository
     * @param string $provider Provider name, e.g. twitter, facebook
     * 
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        try {
            $userSocial = Socialite::driver($provider)->user();

            $user = User::where(['provider_id' => $userSocial->getId()])->first();
            if ($user) {
                if (! $user->provider) {
                    $user->provider = $provider;
                    $user->provider_id = $userSocial->getId();
                    $user->avatar_url = $userSocial->getAvatar();
                    $user->save();
                }
                
                \Auth::login($user);
                
                return redirect()->intended($this->redirectPath());
            } else {
                $dataProvider = [
                    'provider'      => $provider,
                    'provider_id'   => $userSocial->getId(),
                    'avatar_url'    => $userSocial->getAvatar(),
                    'email'         => $userSocial->getEmail(),
                    'name'         => $userSocial->getName()
                ];
                \Session::put('social_provider', $dataProvider);
                return redirect()->route('register');
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }
}
