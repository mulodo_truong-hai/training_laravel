<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Departments;
use App\Repositories\Eloquents\DepartmentRepositoryEloquent;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Collection;

class DepartmentUnitTest extends TestCase
{
    /**
     * Write test case create all department success
     *
     * @return void
     */
    public function testCreateAllDataDepartmentSuccess()
    {
        try {
            // Preparation data
            $data   = [
                'name'        => 'Hoang Phi Hong',
                'description' => 'Danh da man'
            ];

            // execute test
            $deptRepo = new DepartmentRepositoryEloquent();
            $dataRepo = $deptRepo->create($data);
            if (!empty($dataRepo)) {
                // write log
                log::info($dataRepo);
                // get all value in db
                $department = $dataRepo->all();
                foreach ($department as $value) {
                    $dpart['name']          = $value['name'];
                    $dpart['description']   = $value['description'];
                }
            }

            // Result
            $this->assertEquals($dpart['name'], $dataRepo->name);
            $this->assertEquals($dpart['description'], $dataRepo->description);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * Write test case create all department duplicate fails
     *
     * @return void
     */
    public function testCreateAllDataDepartmentDuplicateFails()
    {
        try {
            // Preparation data
            $data   = [
                'name'        => 'Hoang Phi Hong',
                'description' => 'Danh da man'
            ];

            // execute test
            $deptRepo = new DepartmentRepositoryEloquent();
            // get data original
            $dataOriginal = $deptRepo->all();
            foreach ($dataOriginal as $val) {
                $origin['name']         = $val['name'];
                $origin['description']  = $val['description'];
            }
            // check unique name in database
            if ($data['name'] == $origin['name']) {
                // write log
                log::info($dataOriginal);
                $temp = false;
            }

            // result
            $this->assertFalse($temp);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * Write test case update all department success
     *
     * @return void
     */
    public function testUpdateAllDataDepartmentSuccess()
    {
        try {
            // Preparation data
            $data   = [
                'id'            => "13",
                'name'          => 'Ly Lien kiet',
                'description'   => 'Thang cui bap'
            ];

            // execute test
            $deptRepo = new DepartmentRepositoryEloquent();
            // get data original
            $dataOriginal = $deptRepo->update($data['id'], $data);

            // Result
            $this->assertEquals($data['name'], $dataOriginal['name']);
            $this->assertEquals($data['description'], $dataOriginal['description']);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }

    /**
     * Write test case delete one row departmet success
     *
     * @return void
     */
    public function testDeleteOneRowDepartmentSuccess()
    {
        try {
            // execute test
            $deptRepo = new DepartmentRepositoryEloquent();
            // get data original
            $dataOriginal = $deptRepo->delete(12);
            // Result
            $this->assertTrue($dataOriginal);
        } catch (Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
