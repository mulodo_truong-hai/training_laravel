<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAccessModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_access_modules', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedTinyInteger('module_id');
            $table->unsignedTinyInteger('access_level_id');
            $table->timestamps();

            $table->primary(['user_id', 'module_id', 'access_level_id']);

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->foreign('module_id')
                  ->references('id')
                  ->on('modules')
                  ->onDelete('cascade');

            $table->foreign('access_level_id')
                  ->references('id')
                  ->on('module_access_levels')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_access_modules');
    }
}
