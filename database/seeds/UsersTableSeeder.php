<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        // Developers
        DB::table('users')->insert([
            'name' => 'Truong Thanh Hai',
            'email' => 'thanhhaibkit@gmail.com',
            'password' => bcrypt('secret'),
            'email_verified_at' => $now,
            'birthday' => '1982-10-09',
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('users')->insert([
            'name' => 'Tho Nguyen',
            'email' => 'nguyen.tho@mulodo.com',
            'password' => bcrypt('secret')
        ]);

        // Data for testing the user list
        for ($i = 0; $i < 5; $i++) {
            factory(App\User::class, 100)->create();
        }
    }
}
