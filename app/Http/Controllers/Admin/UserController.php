<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class UserController extends Controller
{
    protected $userRepo;

    public function __construct(
        UserRepository $userRepo,
        BuildingMenu $buildingMenu
    ) {
        $this->userRepo = $userRepo;
        $this->buildingMenu = $buildingMenu;

        $this->buildSidebarMenu();
    }

    public function index() {
        // Can use any of following if statement
        //if (\Auth::user()->can('view-user')) {            // via gate
        //if (\Auth::user()->can('view', \Auth::user())) {  // via policy
        if ($this->authorize('view', \App\User::class)) {
            return view('user.index');
        } else {
            return redirect('home');
        }

    }

    public function datatableUserList(Request $request){
        $users = $this->userRepo->getDataTableUsers($request);
        return Datatables::of(
            $this->userRepo->filterUsers($users, $request)
        )
        ->setTotalRecords( $users->count() )
        ->skipPaging()
        ->make(true);
    }

    protected function buildSidebarMenu()
    {
        $menuItems = [
            [
                'text' => 'Test',
                'url' => 'admin.test'
            ],
            [
                'text' => 'Test 2',
                'url' => 'admin.test'
            ]
        ];

        $this->buildingMenu->menu->add(...$menuItems);
    }
}
