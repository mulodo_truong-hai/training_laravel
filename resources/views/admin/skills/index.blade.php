@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Skill infornation</div>
                <!-- @if (session('status.message')) 
                <div class="alert alert-{{ session('status.type') }} alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('status.message') }}
                </div>
                @endif -->
                <div class="card-body">
                    <a class="btn btn-primary" href="{{ route('skills.create') }}">Add Skill</a>
                    <table border="0" class="table">
                        <thead>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Created At</th>
							<th>Updated At</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </thead>
                        <tbody>
                            @foreach ($skills as $skill)
                            <tr>
                                <td>{{ $skill['title'] }}</td>
                                <td>{{ str_limit($skill['description'], 50) }}</td>
                                <td>{{ $skill['created_at'] }}</td>
								<td>{{ $skill['updated_at'] }}</td>
                                <td><i class="fas fa-edit" style="margin-right: 10px;"></i><a class="btn btn-warning" href="{{ route('skills.edit', $skill->id) }}">Edit</a></td>
                                <td>
                                    <form action="{{url('admin/skills', $skill['id'])}}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <i class="fas fa-trash-alt" style="margin-right: 10px;"></i><input type="submit" class="btn btn-danger" value="Delete"/>
                                    </form>
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                <div style="margin: 0 auto;">{{ $skills->links() }}</div>
            </div>
        </div>
    </div>
</div>
@endsection
