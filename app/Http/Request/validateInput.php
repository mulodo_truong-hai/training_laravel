<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Departments;

class validateInput extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(\Symfony\Component\HttpFoundation\Request $request)
    {
        $dept = Departments::find($request->segment(3));

        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
            case 'POST':
            {
                return [
                    'name'          => 'required|string|unique:departments|min:7|max:50|regex:/^[ A-Za-z]+$/',
                    'description'   => 'required|string|max:1024'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'          => 'required|string|min:7|max:50|regex:/^[ A-Za-z]+$/|unique:departments,name,'.$dept->id,
                    'description'   => 'required|string|max:1024'
                ];
            }
            default:
                break;
        }
    }
}
