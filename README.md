# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Laravel Training
* [Learn Laravel](https://laravel.com/docs/5.7/)
* [Learn Laradock](http://laradock.io/)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
```bash
# Update dependencies
$ composer update

# ENV file
$ cp .env.example .env

# Generate App Key
$ php artisan key:generate
```
* Configuration
```ini
# DB
```
* Dependencies
  - [Laravel Socialite](https://laravel.com/docs/5.7/socialite)
  - [Laravel Tinker](https://github.com/laravel/tinker)
  - [DebugBar](https://github.com/barryvdh/laravel-debugbar)
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
  * Hai Truong (truong.hai@mulodo.com)