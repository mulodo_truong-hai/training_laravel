<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccessModule extends Model
{
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id,module_id,access_level_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'module_id', 'access_level_id',
    ];
}
