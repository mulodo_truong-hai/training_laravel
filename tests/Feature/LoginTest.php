<?php

namespace Tests\Feature;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\Data\DataTest;

class LoginTest extends DuskTestCase
{
    /**
     * A dusk test login success.
     *
     * @return void
     */
    public function testLoginSuccess()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', DataTest::INFO['listDepartment']['success']['email'])
                    ->type('password', DataTest::INFO['listDepartment']['success']['password'])
                    ->click('.btn-primary')
                    ->assertSee('Laravel')
                    ->pause(2000);
        });
    }

    /**
     * A dusk test login fails
     *
     * @return void
     */
    public function testLoginFails()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', DataTest::INFO['listDepartment']['fails']['email'])
                    ->type('password', DataTest::INFO['listDepartment']['fails']['password'])
                    ->click('.btn-primary')
                    ->assertSee('These credentials do not match our records.')
                    ->pause(2000);
        });
    }
}
