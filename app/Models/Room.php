<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Room extends Model
{
    /**
     * Return the empty seat of the room
     *
     * --> Move to repository later
     */
    public function emptySeat()
    {
        $seated = User::where('room_id', $this->id)->count();

        return $this->capacity - $seated;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'description', 'capacity',
    ];
}
