<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleAccessLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_access_levels', function (Blueprint $table) {
            $table->unsignedTinyInteger('id')->primary();
            $table->string('code', 8)->unique();
            $table->string('name', 32);
            $table->string('description')->nullable();
            $table->unsignedTinyInteger('value')->nullable()->default(0);
            $table->unsignedTinyInteger('order')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_access_levels');
    }
}
