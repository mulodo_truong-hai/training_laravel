import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/admin/users',
            name: 'users.index',
            component: require("./pages/admin/users/UsersIndex.vue")
        },
        {
            path: '/admin/posts',
            name: 'posts.index',
            component: require('./pages/admin/posts/PostsIndex.vue')
        },
        {
            path: '/admin/posts/create',
            name: 'posts.create',
            component: require('./pages/admin/posts/PostsCreate.vue')
        },
        {
            path: '/admin/posts/:id/edit',
            name: 'posts.edit',
            component: require('./pages/admin/posts/PostsEdit.vue')
        },
        {
            path: '/admin/posts/categories',
            name: 'posts.category',
            component: require('./pages/admin/posts/PostCategory.vue')
        }
    ],
});
