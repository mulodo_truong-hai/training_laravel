<?php

namespace Tests\Data;

class DataTest
{
    const INFO = [
        'listDepartment' => [
            'success'       => [
                'email'             => "manhsangtn@yahoo.com.vn",
                'password'          => "123456",
                'search'            => 'sang',
                'message_Success'   => 'success - Department was delete successful!',
            ],
            'fails'         => [
                'email'             => "gianglong@example.com",
                'password'          => "gianglong123456",
            ],
            'addDepartments'     => [
                'name'           => 'Nguyen Van A',
                'description'    => 'This is Nguyen Van A',
                'message'        => 'success - Department was created successful!',
            ],
            'updateDepartments' => [
                'name'                      => "Dam Van Lua",
                'description'               => 'This is Dam Van Lua',
                'message'                   => 'success - Department was update successful!'
            ],
            'validation'        => [
                'invaild_Name'              => "Nguyen Van A",
                'character_Name'            => 'abc',
                'invaild_description'       => 'This is Nguyen Van A',
                'character_description'     => 'This is abc',
                'error'                     => "The name has already been taken.",
                'error_chracter'            => "The name must be at least 7 characters.",
            ]
        ],
    ];
}
