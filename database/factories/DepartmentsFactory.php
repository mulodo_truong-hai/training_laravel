<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Departments::class, function (Faker $faker) {
    return [
        'name'          => $faker->name,
        'description'   => $faker->paragraph,
    ];
});
