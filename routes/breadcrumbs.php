<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push(trans('Home'), route('home'));
});

// Home > User
Breadcrumbs::for('users', function ($trail) {
    $trail->parent('home');
    $trail->push(trans('adminlte::adminlte.users'), route('admin.users'));
});