<?php

namespace Tests\Feature;

use Tests\Data\DataTest;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ListDepartmentTest extends DuskTestCase
{
    private $url;
    /**
     * funtion setUp
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * A Dusk test list department.
     *
     * @return void
     */
    public function testListDepartment()
    {
        $this->browse(function (Browser $browser) {
            // click button login
            $browser->visit('/login')
                    ->type('email', DataTest::INFO['listDepartment']['success']['email'])
                    ->type('password', DataTest::INFO['listDepartment']['success']['password'])
                    ->click('.btn-primary')
                    ->assertSee('Laravel')
                    ->pause(2000);

            // click button add department case search
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->type('search', DataTest::INFO['listDepartment']['success']['search'])
                    ->pause(2000);

            // click button add department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('#add')
                    ->pause(2000);
            
            // click button edit department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('.glyphicon-pencil')
                    ->pause(2000);

            // click button delete department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('.glyphicon-trash')
                    ->assertSee(DataTest::INFO['listDepartment']['success']['message_Success'])
                    ->pause(2000);
        });
    }

    /**
     * A Dusk test add department success.
     *
     * @return void
     */
    public function testAddDepartmentSuccess()
    {
        $this->browse(function (Browser $browser) {
            // click Add button department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('#add')
                    ->pause(2000);
            
            // get url current in page
            $url = $browser->driver->getCurrentURL();

            // click add button department success
            $browser->visit($url)
                    ->assertSee('Add departments')
                    ->type('name', DataTest::INFO['listDepartment']['addDepartments']['name'])
                    ->type('description', DataTest::INFO['listDepartment']['addDepartments']['description'])
                    ->click('.btn-success')
                    ->assertSee(DataTest::INFO['listDepartment']['addDepartments']['message'])
                    ->pause(2000);
        });
    }

    /**
     * A Dusk test add department fails.
     *
     * @return void
     */
    public function testAddDepartmentFails()
    {
        $this->browse(function (Browser $browser) {
            // click Add button department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('#add')
                    ->pause(2000);
            
            // get url current in page
            $url = $browser->driver->getCurrentURL();

            // click add button department fails.
            // The name has already been taken.
            $browser->visit($url)
                    ->assertSee('Add departments')
                    ->type('name', DataTest::INFO['listDepartment']['validation']['invaild_Name'])
                    ->type('description', DataTest::INFO['listDepartment']['validation']['invaild_description'])
                    ->click('.btn-success')
                    ->assertSee(DataTest::INFO['listDepartment']['validation']['error'])
                    ->pause(2000);
            
            // The name must be at least 7 characters.
            $browser->visit($url)
                    ->assertSee('Add departments')
                    ->type('name', DataTest::INFO['listDepartment']['validation']['character_Name'])
                    ->type('description', DataTest::INFO['listDepartment']['validation']['character_description'])
                    ->click('.btn-success')
                    ->assertSee(DataTest::INFO['listDepartment']['validation']['error_chracter'])
                    ->pause(2000);
        });
    }

    /**
     * A Dusk test update department success.
     *
     * @return void
     */
    public function testUpdateDepartmentSuccess()
    {
        $this->browse(function (Browser $browser) {
            // click edit button department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('.glyphicon-pencil')
                    ->pause(2000);
            
            // get url current in page
            $url = $browser->driver->getCurrentURL();
            
            // click update button department success
            $browser->visit($url)
                    ->assertSee('Edit departments')
                    ->type('name', DataTest::INFO['listDepartment']['updateDepartments']['name'])
                    ->type('description', DataTest::INFO['listDepartment']['updateDepartments']['description'])
                    ->click('.btn-success')
                    ->assertSee(DataTest::INFO['listDepartment']['updateDepartments']['message'])
                    ->pause(2000);
        });
    }

    /**
     * A Dusk test update department fails.
     *
     * @return void
     */
    public function testUpdateDepartmentFails()
    {
        $this->browse(function (Browser $browser) {
            // click edit button department
            $browser->visit('/admin/departments')
                    ->assertSee('List Departments')
                    ->click('.glyphicon-pencil')
                    ->pause(2000);
            
            // get url current in page
            $url = $browser->driver->getCurrentURL();
            // click update button department fails
            // The name has already been taken.
            $browser->visit($url)
                    ->assertSee('Edit departments')
                    ->type('name', DataTest::INFO['listDepartment']['validation']['invaild_Name'])
                    ->type('description', DataTest::INFO['listDepartment']['validation']['invaild_description'])
                    ->click('.btn-success')
                    ->assertSee(DataTest::INFO['listDepartment']['validation']['error'])
                    ->pause(2000);
            
            // The name must be at least 7 characters.
            $browser->visit($url)
                    ->assertSee('Edit departments')
                    ->type('name', DataTest::INFO['listDepartment']['validation']['character_Name'])
                    ->type('description', DataTest::INFO['listDepartment']['validation']['character_description'])
                    ->click('.btn-success')
                    ->assertSee(DataTest::INFO['listDepartment']['validation']['error_chracter'])
                    ->pause(2000);
        });
    }
}
