<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail, HasLocalePreference
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'room_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */
    public function preferredLocale()
    {
        return $this->locale;
    }

    public function getModuleAccessLevel($moduleId)
    {
        $moduleAccess = self::join(
                'user_access_modules',
                'user_access_modules.user_id',
                '=',
                'users.id'
            )
            ->join(
                'module_access_levels',
                'module_access_levels.id',
                '=',
                'user_access_modules.access_level_id'
            )
            ->where('user_access_modules.module_id', $moduleId)
            ->select('module_access_levels.value')
            ->first();

        return $moduleAccess ? $moduleAccess->value : 1;
    }

    // Move to common???
    public function isAdmin()
    {
        return \Auth::user()->is_admin == true;
    }
}
